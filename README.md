First rs commands

```
rsds:PRIMARY> rs.status()
{
	"set" : "rsds",
	"date" : ISODate("2019-10-29T20:23:39.250Z"),
	"myState" : 1,
	"term" : NumberLong(7),
	"syncingTo" : "",
	"syncSourceHost" : "",
	"syncSourceId" : -1,
	"heartbeatIntervalMillis" : NumberLong(2000),
	"majorityVoteCount" : 2,
	"writeMajorityCount" : 2,
	"optimes" : {
		"lastCommittedOpTime" : {
			"ts" : Timestamp(1572380617, 1),
			"t" : NumberLong(7)
		},
		"lastCommittedWallTime" : ISODate("2019-10-29T20:23:37.671Z"),
		"readConcernMajorityOpTime" : {
			"ts" : Timestamp(1572380617, 1),
			"t" : NumberLong(7)
		},
		"readConcernMajorityWallTime" : ISODate("2019-10-29T20:23:37.671Z"),
		"appliedOpTime" : {
			"ts" : Timestamp(1572380617, 1),
			"t" : NumberLong(7)
		},
		"durableOpTime" : {
			"ts" : Timestamp(1572380617, 1),
			"t" : NumberLong(7)
		},
		"lastAppliedWallTime" : ISODate("2019-10-29T20:23:37.671Z"),
		"lastDurableWallTime" : ISODate("2019-10-29T20:23:37.671Z")
	},
	"lastStableRecoveryTimestamp" : Timestamp(1572380607, 1),
	"lastStableCheckpointTimestamp" : Timestamp(1572380607, 1),
	"electionCandidateMetrics" : {
		"lastElectionReason" : "stepUpRequestSkipDryRun",
		"lastElectionDate" : ISODate("2019-10-29T20:23:26.697Z"),
		"termAtElection" : NumberLong(7),
		"lastCommittedOpTimeAtElection" : {
			"ts" : Timestamp(1572380597, 1),
			"t" : NumberLong(6)
		},
		"lastSeenOpTimeAtElection" : {
			"ts" : Timestamp(1572380597, 1),
			"t" : NumberLong(6)
		},
		"numVotesNeeded" : 2,
		"priorityAtElection" : 1,
		"electionTimeoutMillis" : NumberLong(10000),
		"priorPrimaryMemberId" : 1,
		"numCatchUpOps" : NumberLong(1498562884),
		"newTermStartDate" : ISODate("2019-10-29T20:23:27.670Z"),
		"wMajorityWriteAvailabilityDate" : ISODate("2019-10-29T20:23:28.351Z")
	},
	"members" : [
		{
			"_id" : 0,
			"name" : "i1:27017",
			"ip" : "172.31.27.4",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			"uptime" : 68,
			"optime" : {
				"ts" : Timestamp(1572380617, 1),
				"t" : NumberLong(7)
			},
			"optimeDate" : ISODate("2019-10-29T20:23:37Z"),
			"syncingTo" : "",
			"syncSourceHost" : "",
			"syncSourceId" : -1,
			"infoMessage" : "",
			"electionTime" : Timestamp(1572380606, 1),
			"electionDate" : ISODate("2019-10-29T20:23:26Z"),
			"configVersion" : 1,
			"self" : true,
			"lastHeartbeatMessage" : ""
		},
		{
			"_id" : 1,
			"name" : "i2:27017",
			"ip" : "172.31.23.17",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 8,
			"optime" : {
				"ts" : Timestamp(1572380617, 1),
				"t" : NumberLong(7)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1572380617, 1),
				"t" : NumberLong(7)
			},
			"optimeDate" : ISODate("2019-10-29T20:23:37Z"),
			"optimeDurableDate" : ISODate("2019-10-29T20:23:37Z"),
			"lastHeartbeat" : ISODate("2019-10-29T20:23:38.703Z"),
			"lastHeartbeatRecv" : ISODate("2019-10-29T20:23:38.655Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "i3:27017",
			"syncSourceHost" : "i3:27017",
			"syncSourceId" : 2,
			"infoMessage" : "",
			"configVersion" : 1
		},
		{
			"_id" : 2,
			"name" : "i3:27017",
			"ip" : "172.31.23.78",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 13,
			"optime" : {
				"ts" : Timestamp(1572380617, 1),
				"t" : NumberLong(7)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1572380617, 1),
				"t" : NumberLong(7)
			},
			"optimeDate" : ISODate("2019-10-29T20:23:37Z"),
			"optimeDurableDate" : ISODate("2019-10-29T20:23:37Z"),
			"lastHeartbeat" : ISODate("2019-10-29T20:23:38.703Z"),
			"lastHeartbeatRecv" : ISODate("2019-10-29T20:23:38.346Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "i1:27017",
			"syncSourceHost" : "i1:27017",
			"syncSourceId" : 0,
			"infoMessage" : "",
			"configVersion" : 1
		}
	],
	"ok" : 1,
	"$clusterTime" : {
		"clusterTime" : Timestamp(1572380617, 1),
		"signature" : {
			"hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
			"keyId" : NumberLong(0)
		}
	},
	"operationTime" : Timestamp(1572380617, 1)
}
```

```
rsds:PRIMARY> rs.config()
{
	"_id" : "rsds",
	"version" : 1,
	"protocolVersion" : NumberLong(1),
	"writeConcernMajorityJournalDefault" : true,
	"members" : [
		{
			"_id" : 0,
			"host" : "i1:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {
				
			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 1,
			"host" : "i2:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {
				
			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 2,
			"host" : "i3:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {
				
			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		}
	],
	"settings" : {
		"chainingAllowed" : true,
		"heartbeatIntervalMillis" : 2000,
		"heartbeatTimeoutSecs" : 10,
		"electionTimeoutMillis" : 10000,
		"catchUpTimeoutMillis" : -1,
		"catchUpTakeoverDelayMillis" : 30000,
		"getLastErrorModes" : {
			
		},
		"getLastErrorDefaults" : {
			"w" : 1,
			"wtimeout" : 0
		},
		"replicaSetId" : ObjectId("5db864e5f426cf9ad71a456d")
	}
}
```

App:

![first image](https://avatars.mds.yandex.net/get-pdb/2405105/d3c6317c-6156-401a-bf0f-e00408b953bc/s1200)

AFTER SHUTDOWN:

```
rs.status()
{
	"set" : "rsds",
	"date" : ISODate("2019-10-29T20:29:55.845Z"),
	"myState" : 1,
	"term" : NumberLong(10),
	"syncingTo" : "",
	"syncSourceHost" : "",
	"syncSourceId" : -1,
	"heartbeatIntervalMillis" : NumberLong(2000),
	"majorityVoteCount" : 2,
	"writeMajorityCount" : 2,
	"optimes" : {
		"lastCommittedOpTime" : {
			"ts" : Timestamp(1572380987, 1),
			"t" : NumberLong(10)
		},
		"lastCommittedWallTime" : ISODate("2019-10-29T20:29:47.401Z"),
		"readConcernMajorityOpTime" : {
			"ts" : Timestamp(1572380987, 1),
			"t" : NumberLong(10)
		},
		"readConcernMajorityWallTime" : ISODate("2019-10-29T20:29:47.401Z"),
		"appliedOpTime" : {
			"ts" : Timestamp(1572380987, 1),
			"t" : NumberLong(10)
		},
		"durableOpTime" : {
			"ts" : Timestamp(1572380987, 1),
			"t" : NumberLong(10)
		},
		"lastAppliedWallTime" : ISODate("2019-10-29T20:29:47.401Z"),
		"lastDurableWallTime" : ISODate("2019-10-29T20:29:47.401Z")
	},
	"lastStableRecoveryTimestamp" : Timestamp(1572380959, 1),
	"lastStableCheckpointTimestamp" : Timestamp(1572380959, 1),
	"electionCandidateMetrics" : {
		"lastElectionReason" : "electionTimeout",
		"lastElectionDate" : ISODate("2019-10-29T20:29:46.854Z"),
		"termAtElection" : NumberLong(10),
		"lastCommittedOpTimeAtElection" : {
			"ts" : Timestamp(1572380969, 1),
			"t" : NumberLong(8)
		},
		"lastSeenOpTimeAtElection" : {
			"ts" : Timestamp(1572380969, 1),
			"t" : NumberLong(8)
		},
		"numVotesNeeded" : 2,
		"priorityAtElection" : 1,
		"electionTimeoutMillis" : NumberLong(10000),
		"numCatchUpOps" : NumberLong(27017),
		"newTermStartDate" : ISODate("2019-10-29T20:29:47.401Z"),
		"wMajorityWriteAvailabilityDate" : ISODate("2019-10-29T20:29:48.271Z")
	},
	"members" : [
		{
			"_id" : 0,
			"name" : "i1:27017",
			"ip" : "172.31.27.4",
			"health" : 0,
			"state" : 8,
			"stateStr" : "(not reachable/healthy)",
			"uptime" : 0,
			"optime" : {
				"ts" : Timestamp(0, 0),
				"t" : NumberLong(-1)
			},
			"optimeDurable" : {
				"ts" : Timestamp(0, 0),
				"t" : NumberLong(-1)
			},
			"optimeDate" : ISODate("1970-01-01T00:00:00Z"),
			"optimeDurableDate" : ISODate("1970-01-01T00:00:00Z"),
			"lastHeartbeat" : ISODate("2019-10-29T20:29:54.862Z"),
			"lastHeartbeatRecv" : ISODate("1970-01-01T00:00:00Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "Error connecting to i1:27017 (172.31.27.4:27017) :: caused by :: Connection refused",
			"syncingTo" : "",
			"syncSourceHost" : "",
			"syncSourceId" : -1,
			"infoMessage" : "",
			"configVersion" : -1
		},
		{
			"_id" : 1,
			"name" : "i2:27017",
			"ip" : "172.31.23.17",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 14,
			"optime" : {
				"ts" : Timestamp(1572380987, 1),
				"t" : NumberLong(10)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1572380987, 1),
				"t" : NumberLong(10)
			},
			"optimeDate" : ISODate("2019-10-29T20:29:47Z"),
			"optimeDurableDate" : ISODate("2019-10-29T20:29:47Z"),
			"lastHeartbeat" : ISODate("2019-10-29T20:29:54.857Z"),
			"lastHeartbeatRecv" : ISODate("2019-10-29T20:29:54.269Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "i3:27017",
			"syncSourceHost" : "i3:27017",
			"syncSourceId" : 2,
			"infoMessage" : "",
			"configVersion" : 1
		},
		{
			"_id" : 2,
			"name" : "i3:27017",
			"ip" : "172.31.23.78",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			"uptime" : 30,
			"optime" : {
				"ts" : Timestamp(1572380987, 1),
				"t" : NumberLong(10)
			},
			"optimeDate" : ISODate("2019-10-29T20:29:47Z"),
			"syncingTo" : "",
			"syncSourceHost" : "",
			"syncSourceId" : -1,
			"infoMessage" : "could not find member to sync from",
			"electionTime" : Timestamp(1572380986, 1),
			"electionDate" : ISODate("2019-10-29T20:29:46Z"),
			"configVersion" : 1,
			"self" : true,
			"lastHeartbeatMessage" : ""
		}
	],
	"ok" : 1,
	"$clusterTime" : {
		"clusterTime" : Timestamp(1572380987, 1),
		"signature" : {
			"hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
			"keyId" : NumberLong(0)
		}
	},
	"operationTime" : Timestamp(1572380987, 1)
}
```


```
rs.config()
{
	"_id" : "rsds",
	"version" : 1,
	"protocolVersion" : NumberLong(1),
	"writeConcernMajorityJournalDefault" : true,
	"members" : [
		{
			"_id" : 0,
			"host" : "i1:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {
				
			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 1,
			"host" : "i2:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {
				
			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 2,
			"host" : "i3:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {
				
			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		}
	],
	"settings" : {
		"chainingAllowed" : true,
		"heartbeatIntervalMillis" : 2000,
		"heartbeatTimeoutSecs" : 10,
		"electionTimeoutMillis" : 10000,
		"catchUpTimeoutMillis" : -1,
		"catchUpTakeoverDelayMillis" : 30000,
		"getLastErrorModes" : {
			
		},
		"getLastErrorDefaults" : {
			"w" : 1,
			"wtimeout" : 0
		},
		"replicaSetId" : ObjectId("5db864e5f426cf9ad71a456d")
	}
}
```


![second image](https://avatars.mds.yandex.net/get-pdb/1668445/7fabbf80-9650-430e-b8bf-d53b9f0672d8/s1200)