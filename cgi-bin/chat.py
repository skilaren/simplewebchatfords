#!/usr/bin/env python3
import cgi
import html
import pymongo
import datetime


form = cgi.FieldStorage()
name = html.escape(form.getfirst('name', ''))
message = html.escape(form.getfirst('message', ''))

mongoChat = pymongo.MongoClient('i1:27017,i2:27017,i3:27017', replicaset='rsds')
db = mongoChat['dslab']
chat_collection = db['chat_collection']

error = ''

if name and message:
    post = {
        'author': name,
        'message_text': message,
        'timestamp': datetime.datetime.utcnow()
    }
    chat_collection.insert_one(post)
elif name and not message:
    error = f'{name}, you forgot the message!!!'
elif not name and message:
    error = 'Sorry, who are you?'

last_posts = ''
posts_count = 0
for saved_post in chat_collection.find():
    last_posts += f'<p>Time: {saved_post["timestamp"]}  |' \
                  f'  From: {saved_post["author"]}  |' \
                  f'  Message: {saved_post["message_text"]}</p>\n'
    posts_count += 1
    if posts_count > 19:
        break


pattern = f'''
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Chat</title>
</head>
<body>
    <h1> Simple web-chat </h1>
    

    {last_posts}
    
    <br>
    <br>
    
    {error}
    
    Please enter your name and message: 
        <form action="/cgi-bin/chat.py">
            Name: <input type="text" name="name">
            Message: <input type="text" name="message">
            <input type="submit">
        </form>
</body>
</html>
'''

print(pattern)
